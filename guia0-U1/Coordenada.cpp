/*
 * g++ Coordenada.cpp -o Coordenada
 *
 * o ejecutar:
 *
 * make
 */

// Se incluye el formato en formato.h para comenzar a desarrollar el archivo
#include <iostream>
using namespace std;
#include "Coordenada.h"

// Se crean los constructores de la clase y parametros
Coordenada::Coordenada(){
}
// Se definen los metodos de la clase
Coordenada::Coordenada (float x, float y, float z) {
    this-> x = x;
    this-> y = y;
    this-> z = z;
}

// Se crean los metodos se get y set de las coordenadas x, y, z
// Se realizan los get
float Coordenada::get_x() {
    return this->x;
}

float Coordenada::get_y() {
    return this->y;
}

float Coordenada::get_z() {
    return this->z;
}

// Se realizan los set
void Coordenada::set_x(float x) {
    this->x = x;
}

void Coordenada::set_y(float y) {
    this->y = y;
}

void Coordenada::set_z(float z) {
    this->z = z;
}