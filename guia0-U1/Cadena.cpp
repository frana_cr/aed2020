/*
 * g++ Cadena.cpp -o Cadena
 *
 * o ejecutar:
 *
 * make
 */

 /* Se incluye la libreria y el archivo .h general de las clases que son
  * usadas para la construccion del programa*/
#include <list>
#include <iostream>
using namespace std;
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

/* Se crean los constructores de la clase Cadena y parametros */
Cadena::Cadena(string letra) {
    this-> letra = letra;
}

/* métodos get and set de las letras de las cadenas y aminoacidos */
// Se declaran los get
string Cadena::get_letra() {
    return this-> letra;
}

list<Aminoacido> Cadena::get_aminoacidos() {
    return this-> aminoacidos;
}

// Se declaran los set
void Cadena::set_letra(string letra) {
    this-> letra = letra;
}

// Se declara el add que permite añadir los aminoacidos en el menu
void Cadena::add_aminoacido(Aminoacido aminoacido){
	this -> aminoacidos.push_back(aminoacido);
}
