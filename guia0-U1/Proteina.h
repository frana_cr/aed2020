#include <iostream>
using namespace std;
#include "Cadena.h"

#ifndef PROTEINA_H
#define PROTEINA_H

/* Se construye la clase y atributos de la clase Proteina*/
class Proteina {
	/* Se definen atributos privados y publicos*/
	// Se definen los atributos privados
    private:
       string nombre;
       string id;
       list<Cadena> cadenas;
    // Se definen los atributos publicos
    public:
        /* Se definen los constructores */
		Proteina(string id, string nombre);
        /* Métodos get and set */
        string get_id();
        string get_nombre();
        list<Cadena> get_cadenas();
        void set_nombre(string nombre);
        void set_id(string id);
        void add_cadena(Cadena cadena);

};
#endif
