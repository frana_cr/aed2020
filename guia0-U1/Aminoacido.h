#include <list>
#include <iostream>
using namespace std;
#include "Atomo.h"

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

/* Se construye la clase y atributos de la clase Aminoacido*/
class Aminoacido {
	/* Se declaran los atributos publicos y privados*/
	// Se declaran los atributos privados
    private:
       string nombre;
       int numero;
       list<Atomo> atomos;
    // Se declaran los atributos publicos
    public:
        /* Se definen los constructores */
		Aminoacido(string nombre, int numero);

        /* Métodos get and set */
        string get_nombre();
        int get_numero();
        list <Atomo> get_atomos();
        void set_nombre(string nombre);
        void set_numero(int numero);
        void add_atomo(Atomo atomo);
};
#endif