// Se realiza en formato h para ser la cabecera del documento .cpp
#include <iostream>
using namespace std;

#ifndef COORDENADA_H
#define COORDENADA_H

// Se define la clase coordenada y los atributos publicos y privados
class Coordenada {
	// Atributos privados con su tipo
    private:
       float x;
       float y;
       float z;
	// Atributos publicos
    public:
        // Se definen los constructores
        Coordenada ();
        Coordenada (float x, float y, float z);

        // Se definen los metodos get y set
        float get_x();
        float get_y();
        float get_z();
        void set_x(float x);
        void set_y (float y);
        void set_z (float z);
};
#endif