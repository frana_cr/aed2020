/*
 * g++ Proteina.cpp -o Proteina
 *
 * o ejecutar:
 *
 * make
 */

 /* Se incluye la libreria y el archivo .h general de la clase y los
  *  archivos que tambien son parte de la construccion del programa*/
#include <list>
#include <iostream>
using namespace std;
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

/* Se crean los constructores de la clase Proteina y parametros*/
Proteina::Proteina(string id, string nombre) {
    this-> id = id;
    this-> nombre = nombre;
}

/* métodos get and set del id, nombre y cadenas que son parte de las proteinas */
// Se declaran los get
string Proteina::get_id() {
    return this-> id;
}

string Proteina::get_nombre() {
    return this-> nombre;
}

list <Cadena> Proteina::get_cadenas() {
    return this-> cadenas;
}
// Se declaran los set
void Proteina::set_nombre(string nombre) {
    this-> nombre = nombre;
}

void Proteina::set_id(string id) {
    this-> id = id;
}
// Se declara el add que permite añadir objetos a esta clase para el menu
void Proteina :: add_cadena(Cadena cadena){
	this -> cadenas.push_back(cadena);
}