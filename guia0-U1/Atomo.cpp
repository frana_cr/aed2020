/*
 * g++ Atomo.cpp -o Atomo
 *
 * o ejecutar:
 *
 * make
 */

 /* Se incluye la libreria y el archivo .h general de la clase*/
#include <iostream>
using namespace std;
#include "Atomo.h"
#include "Coordenada.h"

/* Se crean los constructores de la clase Atomo y sus parametros*/
Atomo::Atomo (string nombre, int numero) {
    this-> nombre = nombre;
    this-> numero = numero;
	//this-> coordenada = Coordenada(x,y,z);
}

/* métodos get and set del nombre, numero y coordenada del atomo */
// Se realizan los get
string Atomo::get_nombre() {
    return this-> nombre;
}

int Atomo::get_numero() {
    return this-> numero;
}

Coordenada Atomo::get_coordenada() {
    return this-> coordenada;
}

// Se realizan los set
void Atomo::set_nombre(string nombre) {
    this-> nombre = nombre;
}

void Atomo::set_numero(int numero) {
    this-> numero = numero;
}

void Atomo::set_coordenada(Coordenada coordenada){
	this-> coordenada = coordenada;
}