/*
 * g++ Aminoacidos.cpp -o Aminoacido
 *
 * o ejecutar:
 *
 * make
 */

 /* Se incluye la libreria y el archivo .h general de la clase, con otros
  * archivos que son parte de las clases que se usaran, se incluye ademas el formato lista */
#include <list>
#include <iostream>
using namespace std;
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

/* Se crean los constructores de la clase Aminoacido y parametros */

Aminoacido::Aminoacido (string nombre, int numero) {
    this->nombre = nombre;
    this->numero = numero;
}

/* métodos get and set del nombre, numero y atomos de los aminoacidos */
// Se declaran los get
string Aminoacido::get_nombre() {
    return this-> nombre;
}

int Aminoacido::get_numero() {
    return this-> numero;
}

list <Atomo> Aminoacido:: get_atomos(){
	return this-> atomos;
}
// Se declaran los set
void Aminoacido::set_nombre(string nombre) {
    this-> nombre = nombre;
}
// Se declara el add para añadir de esta manera los atomos en el menu
void Aminoacido::add_atomo(Atomo atomo){
	this -> atomos.push_back(atomo);
}