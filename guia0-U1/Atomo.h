#include <iostream>
using namespace std;
#include "Coordenada.h"

#ifndef ATOMO_H
#define ATOMO_H

//Se construye la clase y atributos
class Atomo{
/* Se declaran los atributos publicos y privados*/
	// Atributos privados
    private:
       string nombre;
       int numero;
       Coordenada coordenada;
    // Atributos publicos
    public:
        /* Se definen los constructores */
		Atomo(string nombre, int numero);

        /* Métodos get and set */
        string get_nombre();
        int get_numero();
        Coordenada get_coordenada();
        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_coordenada(Coordenada coordenada);
};
#endif
