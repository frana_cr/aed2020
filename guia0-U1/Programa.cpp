/*
 * g++ Programa.cpp -o Programa
 *
 * o ejecutar:
 *
 * make
 */
// Se incluyen las listan y archivos en formato .h que son parte de la construccion del programa
#include <list>
#include <iostream>
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

/* LA FUNCIONALIDAD DE ESTA FUNCION RADICA EN QUE PERMITE OBTENER Y PRESENTAR COMO LOS DATOS SERAN IMPRESOS*/
void imprimir_datos_proteinas(Proteina p){
    // MUESTRA LAS PROTEINAS
    cout << "Id:" << p.get_id() << "Nombre:" << p.get_nombre() << endl;
    // POR MEDIO DEL FOR SE PUEDEN RECORRER LAS CLASES E IMPRIMIR
    // MUESTRA LA CADENA
    for(Cadena cadena: p.get_cadenas()){
        cout << "Cadena:" << cadena.get_letra() << endl;
        // MUESTRA LOS AMINOACIDOS
        for(Aminoacido aminoacido: cadena.get_aminoacidos()){
            cout << "Nombre:" << aminoacido.get_nombre() << endl;
            cout << "Numero:" << aminoacido.get_numero() << endl;
                // MUESTRA TODO REFERENTE AL ATOMO Y COORDENADAS
            for(Atomo atomo: aminoacido.get_atomos()){
                cout << "Nombre:" << atomo.get_nombre() << endl;
                cout << "Numero:" << atomo.get_numero() << endl;
                cout << "Coordenada X:" << atomo.get_coordenada().get_x() << endl;
                cout << "Coordenada Y:" << atomo.get_coordenada().get_y() << endl;
                cout << "Coordenada Z:" << atomo.get_coordenada().get_z()<< endl;
            }
        }
    }
}
// EN ESTA FUNCION EL USUARIO DEBE INGRESAR LOS DATOS QUE SON SOLICITADOS
void leer_datos_proteinas(){
    list<Proteina> proteinas;
    string nombre_proteina, id_proteina, cadena_proteina, nombre_amino, numero_amino, nombre_atomo, numero_atomo, coordenadaX, coordenadaY, coordenadaZ;
    // SE CONSULTA POR LOS DATOS A LOS USUARIOS SOBRE ID, NOMBRE, NUMERO, COORDENADAS, ENTRE OTROS
    cout << "ID de la proteina:";
    getline(cin, id_proteina);

    cout << "NOMBRE de la proteina:";
    getline(cin, nombre_proteina);

    cout << "LETRAS de la cadena:";
    getline(cin, cadena_proteina);

    cout << "NOMBRE del aminoacido:";
    getline(cin, nombre_amino);

    cout << "NUMERO del aminoacido";
    getline(cin, numero_amino);

    cout << "NOMBRE del atomo";
    getline(cin, nombre_atomo);

    cout << "NUMERO del atomo";
    getline(cin, numero_atomo);

    cout << "COORDENADA X:";
    getline(cin, coordenadaX);

    cout << "COORDENADA Y";
    getline(cin, coordenadaY);

    cout << "COORDENADA Z :";
    getline(cin, coordenadaZ);

    // SE CREAN LOS OBJETOS Y SE AÑADEN A LOS RESPECTIVOS SET
    // OBJETO PROTEINAS
    Proteina prot1 = Proteina(nombre_proteina, id_proteina);
    prot1.set_id(id_proteina);
    prot1.set_nombre(nombre_proteina);

	// OBJETO CADENA
    Cadena cad1 = Cadena(cadena_proteina);
    prot1.set_letra(cadena_proteina);
    // DE ESTA MANERA SE AGREGAN
    prot1.add_cadena(cad1);

    // OBJETO AMINOACIDO SE OCUPA STOI DEBIDO A QUE CONVIERTE EL STRING A ENTERO
    Aminoacido amino1 = Aminoacido(nombre_amino, stoi(numero_amino));
	prot1.set_nombre(nombre_amino);
	prot1.set_numero(numero_amino);
	// SE AÑADE A LA CADENA EL AMINOACIDO
	cad1.add_aminoacido(amino1);

	//OBJETO ATOMO
    Atomo atom1 = Atomo(nombre_atomo, stoi(numero_atomo));
    prot1.set_nombre(nombre_atomo);
    prot1.set_numero(numero_atomo);
    // SE AÑADE AL AMINOACIDO EL ATOMO
    amino1.add_atomo(atom1);

    // OBJETO COORDENADA, SE OCUPA STOF DEBIDO A QUE CONVIERTE EL STRING A FLOAT
    Coordenada coord1 = Coordenada(stof(coordenadaX), stof(coordenadaY), stof(coordenadaZ));
    prot1.set_x(coordenadaX);
    prot1.set_y(coordenadaY);
    prot1.set_z(coordenadaZ);
    // SE AÑADEN LAS COORDENADAS DEL ATOMO
    atom1.set_coordenada(coord1);

    // TAMBIEN SE IMPRIME UNA PROTEINA DE EJEMPLO Y SE ESTABLECE DE MODO DIRECTO
    //Proteina p2 = Proteina("1A1Y", "ALCALASE (SUBTILISIN CARLSBERG VARIANT)", "AATGCC", 4567, "ALA", 1234, "NOO", "%13.01", "%45.06", "%65.07");

    // LOS DATOS DE LAS PROTEINAS SON IMPRESOS
    imprimir_datos_proteinas(prot1);
    //imprimir_datos_proteinas(p2);
}
/*EN EL MENU, SE SOLICITA AL USUARIO ESCOGER LA OPCION QUE DESEA,
 * TAMBIEN SE PUEDE OBTIENE LA INFORMACION DE LAS PROTEINAS*/

int main (void) {
    // SE INGRESA LA OPCION DE UN TIPO ESPECIFICO
    int opcion;
    // SE MUESTRA LA INFORMACION QUE SALDRA EN PANTALLA
    cout << " INFORMACION PROTEINAS" << endl;
    cout << " MENU DE OPCIONES \n" << endl;
    cout << " OPCION 1, Agregar proteina \n" << endl;
    cout << " OPCION 2, Salir del programa \n" << endl;
    // SE SOLICITA INGRESAR LA OPCION
    cout << "Ingrese su opcion:";
    cin >> opcion;
    // SI LA OPCION ES EL CASO 1, SE AGREGA INFORMACION.
    switch (opcion){
        case 1:
        cout << "Agregagando informacion\n";
        leer_datos_proteinas();
        break;
        // SI LA INFORMACION ES EL CASO 2, EL PROGRAMA SE CERRARA.
        case 2:
        cout << "El programa se esta cerrando \n";
        exit(0);
        }
        return 0;
}