#include <list>
#include <iostream>
using namespace std;
#include "Aminoacido.h"

#ifndef CADENA_H
#define CADENA_H

/* Se construye la clase y atributos de la clase cadena*/
class Cadena {
		/* Se declaran los atributos publicos y privados*/
	// Se declaran los atributos privados
    private:
       string letra;
       list<Aminoacido> aminoacidos;
    // Se declaran los atributos publicos
    public:
        /* Se definen los constructores */
		Cadena(string letra);
        /* Métodos get and set */
        string get_letra();
        list <Aminoacido> get_aminoacidos();
        void set_letra(string letra);
        void add_aminoacido(Aminoacido aminoacido);
};
#endif
