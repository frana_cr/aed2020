# GUIA 0.1 AED "Introduccion a C++"
El propósito de la realización de esta guía es que mediante un diagrama de clases escribir un programa en C++, para poder comprender como funcionan las clases, atributos y cómo se construye un programa. Esto permitirá el ingreso de proteinas y de información referente a ella como por ejemplo sus aminoácidos, cadenas, átomo y coordenadas.

El desarrollo de esta guía consiste en construir un programa en lenguaje C++ a partir de un diagrama en formato dia, esto con la finalidad de poder obtener información de las proteínas y de cierta información básica de ellas, por medio de la clase principal programa se puede obtener el desarrollo de las demás clases que permiten las acciones. Cada clase contiene ciertos atributos los que permiten que la funcionalidad del programa, estas clases están conectadas por medio de la herencia, cada clase contiene además atributos propios los cuales permiten las acciones de las siguientes clases que heredan características tambien las clases tienen ciertos atributos de caracter privado y publico los cuales dependiendo su estado pueden tener acceso. Los archivos están escritos en formato .cpp y .h este último tiene la funcionalidad de ser archivos de cabecera como código fuente para hacer los procesos más automáticos es por esto que cada clase debe contener su archivo . h. También se añade un menú para poder, ingresar una proteína y salir del programa. Esta ademas con su archivo Makefile el cual permite ejecutar el programa desde la terminal.

Las clases de este programa son:
-Coordenadas: Contiene como atributo de caracter privado las coordenadas x, y, z. 
-Atomos: Sus atributos son nombre, numero y las coordenadas.
-Aminoacidos: Sus atributos son nombre, numero y la lista de los atomos que son parte.
- Cadena: La cual contiene como atributos la lista de los aminoacidos y las letras.
- Proteina: Esta contiene los atributos del Id y nombre de la proteina que se desea ingresar, almacena los datos referente a las proteinas y la lista de las cadenas.
-Programa: Se desarrolla el menu y se lleva a cabo la interaccion con el usuario debido a que tiene que ingresar ciertos datos para la funcionalidad del programa. 
El programa solicita al usuario los datos para luego se creen los objetos y sean añadidos dentro de la funcion leer_datos_proteinas, finalmente por medio de la funcion imprimir_datos_proteina mostrar al usuario lo que se ingreso.
- El programa no logra compilar o ser ejectutado con exito, pero los archivos .cpp y .h de las clases si , tambien se probaron las opciones del menu y estas funcionan de buena manera, se cree que el problema de compilacion puede ser referente a las clases descritas con anterioridad por un mal manejo de estas y de sus datos, tambien se probo de manera separada si funcionan las instancias para ingresar datos y efectivamente realizan su mision. 

## Pre-requisitos 
Sistema operativo Linux versión igual o superior a 18.04
Editor de texto (vim o geany)

#### Instalación

Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:
lsb_release -a (versión de Ubuntu)
En donde:
Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando.

Para descargar un editor de texto como vim se puede descargar de la siguiente manera:
sudo apt install vim
En donde:
Por medio de este editor de texto se puede construir el codigo 
- En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.

## Ejecutando las pruebas 
Para implementar la ejecución del código, colocar en terminal: g++ Programa -o Programa o por medio del archivo Makefile. 
- Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo.

## Construido con:
- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Geany: Editor de código.

## Versiones
Versiones de herramientas:
Ubuntu 20.04 LTS
Geany 1.36-1build1
Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/aed2020/-/tree/master/guia0-U1

## Autores 
Francisca Castillo - Desarrollo del código y proyecto, narración README.

## Expresiones de Gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=730550
Al ayudante del modulo por explicar y solucionar dudas referentes al desarrollo de la guia . 
A contenido de internet  del sitio programar y ya, leccion impartida por Juan David Meza González:https://www.programarya.com/Cursos/C++/Entrada-y-Salida-de-Datos 


