// Se agrega el include, la clase , gracias al using es mucho mas eficiente
#include <iostream>
using namespace std;

#ifndef CALCULOS_H
#define CALCULOS_H
// SE ANADE LA CLASE CALCULOS CON SUS ATRIBUTOS PUBLICOS Y PRIVADOS
class Calculos {
	// Atributos privados 
	private:
	int numeros = 0;
	int tamano_arreglo = 0;
	// Atributos publicos con su respectivo constructor
	public:
	Calculos();
	Calculos(int numeros, int tamano_arreglo);

	// HACER LOS METODOS SET Y GET (PENDIENTE)
};