# LAB 1 - EJERCICIO 1 "SUMA DEL CUADRADO DE UN NUMERO"
## Comenzando
El proposito de este ejercicio es realizar un codigo en lenguaje C++, mediante la implementacion de clases crear un programa el cual permita llenar un areglo de caracter unidimensional con numeros enteros para luego ejecutar la accion de calcular la suma del cuadrado de los numeros, cabe destacar que la medida del arreglo de N elementos.  
### Prerequisitos
- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (vim o geany)
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:
lsb_release -a (versión de Ubuntu)
En donde:
Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando.
Para descargar un editor de texto como vim se puede descargar de la siguiente manera:
- sudo apt install vim
En donde: Por medio de este editor de texto se puede construir el codigo.
- En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.
### Instalacion
## Ejecutando Pruebas
## Construido con
- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Geany: Editor de código.
## Versiones
Versiones de herramientas:
- Ubuntu 20.04 LTS
- Geany 1.36-1build1
- Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/aed2020/-/tree/master/Lab1%20-%202%20OCT
## Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.
## Expresiones de gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes:  https://lms.educandus.cl/mod/lesson/view.php?id=730534